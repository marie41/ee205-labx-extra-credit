///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 10x - Collection Class Evaluator
///
/// @file containers.cpp
/// @version 1.0
///
/// Collection class testing code
///
/// @author Marie Wong <marie4@hawaii.edu>
/// @brief  Lab 10x - Collection Class Evaluator - EE 205 - Spr 2021
/// @date   14_MAY_2021
///////////////////////////////////////////////////////////////////////////////

#include "helper.cpp"


//////////////////////////////////  Vector  //////////////////////////////////
///
/// This class wraps a vector<long> container and has methods to:
///   - Initialize the vector
///   - Insert data into the vector (measuring the insertion time)
///   - Clear data out of the vector
///   - Search for data in the vector
class TestVector : public ABSTRACT_TEST_CLASS {
private:
	static vector<long> container;

public:
	virtual inline void initDataStructure() __attribute__((always_inline)) { container.clear(); }

	virtual inline ticks_t testInsert() __attribute__((always_inline)) {
		auto endIterator = container.end();
		insert_value = rand();

		MARK_TIME( start );

		// Do the operation ///////
		container.insert( endIterator, insert_value );
		///////////////////////////

		MARK_TIME( end );

		return ( DIFF_TIME( start, end ) );
	}

	virtual inline ticks_t clearDataStructure()   __attribute__((always_inline)) {
		MARK_TIME( start );

		container.clear();

		MARK_TIME( end );

		return ( DIFF_TIME( start, end ) );
	}

	virtual inline ticks_t testSearch()   __attribute__((always_inline)) {
		search_value = rand();

		MARK_TIME( start );

		for( auto i : container ) {
			if( i == search_value )
				break;
		}

		MARK_TIME( end );

		return ( DIFF_TIME( start, end ) );
	}

}; // Vector

vector<long> TestVector::container;



//////////////////////////////////  List  //////////////////////////////////
// Same as above
class TestList : public ABSTRACT_TEST_CLASS {
private:
	static list<long> container;

public:
	virtual inline void initDataStructure() __attribute__((always_inline)) { container.clear(); }

	virtual inline ticks_t testInsert() __attribute__((always_inline)) {
		auto endIterator = container.end();
		insert_value = rand();

		MARK_TIME( start );

		// Do the operation ///////
		container.insert( endIterator, insert_value );
		///////////////////////////

		MARK_TIME( end );

		return ( DIFF_TIME( start, end ) );
	}

	virtual inline ticks_t clearDataStructure()   __attribute__((always_inline)) {
		MARK_TIME( start );

		container.clear();

		MARK_TIME( end );

		return ( DIFF_TIME( start, end ) );
	}

	virtual inline ticks_t testSearch()   __attribute__((always_inline)) {
		search_value = rand();

		MARK_TIME( start );

		for( auto i : container ) {
			if( i == search_value )
				break;
		}

		MARK_TIME( end );

		return ( DIFF_TIME( start, end ) );
	}

}; // List

list<long> TestList::container;



//////////////////////////////////  Set  //////////////////////////////////
// Same as above
class TestSet : public ABSTRACT_TEST_CLASS {
private:
	static set<long> container;

public:
	virtual inline void initDataStructure() __attribute__((always_inline)) { container.clear(); }

	virtual inline ticks_t testInsert() __attribute__((always_inline)) {
		auto endIterator = container.end();
		insert_value = rand();

		MARK_TIME( start );

		// Do the operation ///////
		container.insert( endIterator, insert_value );
		///////////////////////////

		MARK_TIME( end );

		return ( DIFF_TIME( start, end ) );
	}

	virtual inline ticks_t clearDataStructure()   __attribute__((always_inline)) {
		MARK_TIME( start );

		container.clear();

		MARK_TIME( end );

		return ( DIFF_TIME( start, end ) );
	}

	virtual inline ticks_t testSearch()   __attribute__((always_inline)) {
		search_value = rand();

		MARK_TIME( start );
      
      container.find( search_value );
		
      MARK_TIME( end );

		return ( DIFF_TIME( start, end ) );
	}

}; // Set

set<long> TestSet::container;



//////////////////////////////////  Map  //////////////////////////////////
// Same as above
class TestMap : public ABSTRACT_TEST_CLASS {
private:
	static map<long, long> container;

public:
	virtual inline void initDataStructure() __attribute__((always_inline)) { container.clear(); }

	virtual inline ticks_t testInsert() __attribute__((always_inline)) {
		auto endIterator = container.end();
		insert_value = rand();

		MARK_TIME( start );

		// Do the operation ///////
		container.insert( endIterator, pair<long, long>( insert_value, insert_value ) );
		///////////////////////////

		MARK_TIME( end );

		return ( DIFF_TIME( start, end ) );
	}

	virtual inline ticks_t clearDataStructure()   __attribute__((always_inline)) {
		MARK_TIME( start );

		container.clear();

		MARK_TIME( end );

		return ( DIFF_TIME( start, end ) );
	}

	virtual inline ticks_t testSearch()   __attribute__((always_inline)) {
		search_value = rand();

		MARK_TIME( start );

      container.find( search_value );
      
		MARK_TIME( end );

		return ( DIFF_TIME( start, end ) );
	}

}; // Map

map<long, long> TestMap::container;



//////////////////////////////////  Unordered Map  //////////////////////////////////
// Same as above
class TestUnorderedMap : public ABSTRACT_TEST_CLASS {
private:
	static unordered_map<long, long> container;

public:
	virtual inline void initDataStructure() __attribute__((always_inline)) { container.clear(); }

	virtual inline ticks_t testInsert() __attribute__((always_inline)) {
		insert_value = rand();

		MARK_TIME( start );

		// Do the operation ///////
		container.insert( pair<long, long>( insert_value, insert_value ) );
		///////////////////////////

		MARK_TIME( end );

		return ( DIFF_TIME( start, end ) );
	}

	virtual inline ticks_t clearDataStructure()   __attribute__((always_inline)) {
		MARK_TIME( start );

		container.clear();

		MARK_TIME( end );

		return ( DIFF_TIME( start, end ) );
	}

	virtual inline ticks_t testSearch()   __attribute__((always_inline)) {
		search_value = rand();

		MARK_TIME( start );

		for( auto& [key, value]: container ) {
			if( value == search_value )
				break;
		}

		MARK_TIME( end );

		return ( DIFF_TIME( start, end ) );
	}

}; // Unordered Map

unordered_map<long, long> TestUnorderedMap::container;



//////////////////////////////////  Unordered Set  //////////////////////////////////
// Same as above
class TestUnorderedSet : public ABSTRACT_TEST_CLASS {
private:
	static unordered_set<long> container;

public:
	virtual inline void initDataStructure() __attribute__((always_inline)) { container.clear(); }

	virtual inline ticks_t testInsert() __attribute__((always_inline)) {
		insert_value = rand();

		MARK_TIME( start );

		// Do the operation ///////
		container.insert( insert_value );
		///////////////////////////

		MARK_TIME( end );

		return ( DIFF_TIME( start, end ) );
	}

	virtual inline ticks_t clearDataStructure()   __attribute__((always_inline)) {
		MARK_TIME( start );

		container.clear();

		MARK_TIME( end );

		return ( DIFF_TIME( start, end ) );
	}

	virtual inline ticks_t testSearch()   __attribute__((always_inline)) {
		search_value = rand();

		MARK_TIME( start );

		container.find( search_value );

		MARK_TIME( end );

		return ( DIFF_TIME( start, end ) );
	}

}; // Unordered Set

unordered_set<long> TestUnorderedSet::container;


