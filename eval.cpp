///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 10x - Collection Class Evaluator
///
/// @file eval.cpp
/// @version 1.0
///
/// Evaluate a number of Standard C++ Collection classes for performance
///
/// @author < Marie Wong marie4@hawaii.edu>
/// @brief  Lab 10x - Collection Class Evaluator - EE 205 - Spr 2021
/// @date   14_MAY_2021
///////////////////////////////////////////////////////////////////////////////

#include "helper.cpp"
#include "containers.cpp"

using namespace std;


///////////////////////////////////  main  ///////////////////////////////////
int main() {
	cout << "Welcome to the Gnu C++ Collection Class Evaluator" << endl;

	static long start, end;
	for( unsigned int i = 0 ; i < CALIBRATE_OVERHEAD ; i++ ) {
		MARK_TIME( start );
		MARK_TIME( end );
		TEST_OVERHEAD = min( TEST_OVERHEAD, (end - start) );
	}

	cout << "Approximate test overhead is: " << TEST_OVERHEAD << endl;

   Result::printResultsHeader();

   // Vectors
   TestVector testVector;
   Result vectorResult( &testVector, "vector" );
   vectorResult.testDataStructure();
   vectorResult.printResults();

   // Lists
   TestList testList;
   Result listResult( &testList, "list" );
   listResult.testDataStructure();
   listResult.printResults();

   // Sets
   TestSet testSet;
   Result setResult( &testSet, "set" );
   setResult.testDataStructure();
   setResult.printResults();

   // Maps
   TestMap testMap;
   Result mapResult( &testMap, "map" );
   mapResult.testDataStructure();
   mapResult.printResults();
  
   // Unordered Maps
   TestUnorderedMap testUnorderedMap;
   Result unorderedMapResult( &testUnorderedMap, "unordered map" );
   unorderedMapResult.testDataStructure();
   unorderedMapResult.printResults();

   // Unordered Sets
   TestUnorderedSet testUnorderedSet;
   Result unorderedSetResult( &testUnorderedSet, "unordered set" );
   unorderedSetResult.testDataStructure();
   unorderedSetResult.printResults();
  

} // main()
